<?php
include './base.php';

if (isset($a['cookie']) && !isset($a['username']) && !isset($a['password']) && !isset($a['lorem']))
{
    $cookie = $a['cookie'];
    $cookie_file_path = "${PR_PATH}$cookie";
    $cookie_exists = file_exists($cookie_file_path);
    if ($cookie_exists)
        $cookie_file_content = file_get_contents($cookie_file_path);
    else
        die_result(0, $ERR_INVALID_COOKIE);

    if ($cookie_exists && $cookie_file_content == $_SERVER['REMOTE_ADDR'])
    {
        $sql = connectSQL();
        unset($cookie_file_path);
        completeRegistration($sql, $cookie);
    }
    else
        die_result(0, $ERR_INVALID_COOKIE_OWNER);
}

req(array('username', 'password', 'lorem'), $a);

$username = $a['username'];
$password = $a['password'];
$lorem = $a['lorem'];

$sql = connectSQL();

if (mysqli_num_rows($sql->query("SELECT * FROM `users` WHERE `username`=\"$username\"")))
    die_result(0, $ERR_DUPLICATE);
else
{
    $cookie = addPendingRegistration($sql, $username, $password, $lorem);
    die_result(0, $SUCCESS, array('cookie' => $cookie));
}

$sql->close();

function addPendingRegistration($sql, $username, $password, $lorem)
{
    global $PR_PATH;
    global $ERR_REGISTER;
    global $ERR_DUPLICATE;

    if (mysqli_num_rows($sql->query("SELECT * FROM `registration_pending` WHERE `username`=\"$username\"")))
        die_result(0, $ERR_DUPLICATE);

    $cookie = getRandomString();
    if ($sql->query("INSERT INTO `registration_pending`(`id`, `username`, `password`, `lorem`, `lastsessionid`) VALUES ('" . genId() . "','$username','$password','$lorem','$cookie')"))
    {
        $file = fopen("${PR_PATH}$cookie", "w");
        fwrite($file, $_SERVER['REMOTE_ADDR']);
        fclose($file);
    }
    else
        die_result(0, $ERR_MYSQL);

    return $cookie;
}

function completeRegistration($sql, $cookie)
{
    global $PR_PATH;
    global $PR_TIMEOUT;
    global $ERR_INVALID_COOKIE;
    global $ERR_EXPIRED_COOKIE;
    global $SUCCESS;
    global $ERR_INVALID_COOKIE_OWNER;

    $cookie_file_path = "${PR_PATH}$cookie";
    $cookie_exists = file_exists($cookie_file_path);
    if ($cookie_exists)
        $cookie_file_content = file_get_contents($cookie_file_path);
    else
        die_result(0, $ERR_INVALID_COOKIE);

    if ($cookie_exists && $cookie_file_content == $_SERVER['REMOTE_ADDR'])
    {
        $time = time()-filemtime($cookie_file_path);
        unlink($cookie_file_path);
        if ($time > $PR_TIMEOUT)
        {
            $sql->query("DELETE FROM `registration_pending` WHERE `lastsessionid`='$cookie'");
            die_result(0, $ERR_EXPIRED_COOKIE);
        }

        if ($sql->query("INSERT INTO `users` SELECT * FROM `registration_pending` WHERE `lastsessionid`='$cookie'"))
        {
            $sql_events = connectSQL('theapp_events', 'localhost', 'root', '');
            $sql_friends = connectSQL('theapp_friends', 'localhost', 'root', '');
            $sql->query("DELETE FROM `registration_pending` WHERE `lastsessionid`='$cookie'");
            $r = sqlsel($sql, "SELECT * FROM `users` WHERE `lastsessionid`='$cookie'");
            $id = $r[0]['id'];
            $sql_events->query("CREATE TABLE `$id` (
                `id` int(11) NOT NULL,
                `name` varchar(20) NOT NULL,
                `date` varchar(20) NOT NULL,
                `friends_invited` varchar(300) NOT NULL,
                `date_created` varchar(20) NOT NULL,
                 UNIQUE (`id`)
            )");
            $sql_friends->query("CREATE TABLE `$id` (
                `id` int(11) NOT NULL,
                `nickname` varchar(20) NOT NULL,
                `date_added` varchar(20) NOT NULL,
                 UNIQUE (`id`)
            )");

            die_result(0, $SUCCESS);
        }
        else
        {
            $sql->query("DELETE FROM `registration_pending` WHERE `lastsessionid`='$cookie'");
            die_result(0, $ERR_REGISTER);
        }
    }
    else
        die_result(0, $ERR_INVALID_COOKIE_OWNER);
}

?>
