<?php

$a = $_GET;

$PR_PATH                    = "pending/";
$PR_TIMEOUT                 = 60;           // def: 60s

$COOKIE_PATH                = "sessions/";
$COOKIE_LENGTH              = 4;            // def: 40 characters
$COOKIE_TIMEOUT             = 10000 * 10;   // def: 10s
$COOKIE_OVERWRITE_TIMEOUT   = 10;           // def:300s

$REQ_USER_INFO              = 1;
$REQ_EVENTS                 = 5;
$REQ_FRIEND_LIST            = 4;

$SUCCESS                    = "1";
$ERR                        = "-1";
$ERR_INVALID_COOKIE         = "-2";
$ERR_EXPIRED_COOKIE         = "-27";
$ERR_INVALID_COOKIE_OWNER   = "-23";
$ERR_DUPLICATE_LOGIN        = "-30";
$ERR_DUPLICATE              = "-66";
$ERR_INVALID_ARGUMENTS      = "-7";
$ERR_INVALID_LOGIN          = "-75";
$ERR_MYSQL                  = "-88";
$ERR_MYSQL_TABLE            = "-85";
$ERR_REGISTER               = "-55";

function checkCookie($cookie) // TODO: otherwise you can use all other functions with expired cookie or non existent cookie !!
{

}

function result($depth, $code, $data='')
{
    global $DEBUG;

    if ($data != '')
        $array = array('depth' => $depth, 'code' => $code, 'data' => $data);
    elseif (is_array($data))
        $array = array('depth' => $depth, 'code' => $code, $data);
    else
        $array = array('depth' => $depth, 'code' => $code);

    $json = json_encode($array, JSON_PRETTY_PRINT);
    echo $json;
}

function die_result($depth, $code, $data='')
{
    die(result($depth, $code, $data));
}

function connectSQL($db='theapp', $host='localhost', $username='alex', $password='')
{
    global $ERR_MYSQL;
    $sql = new mysqli($host, $username, $password, $db);
    if ($sql->connect_error)
    {
        die_result(0, $ERR_MYSQL);
    }
    return $sql;
}

function sqlsel($sql, $query, $die_msg='')
{
    global $ERR_MYSQL;
    $result = $sql->query($query);
    if ($result == NULL)
        return NULL;
    
    if (mysqli_num_rows($result) <= 0)
    {
        return NULL;
    }

    $rows = array();
    while($r = mysqli_fetch_assoc($result))
    {
        $rows[] = $r;
    }
    return $rows;
}

function getRandomString()
{
    global $COOKIE_LENGTH;
    return substr(str_shuffle(implode(array_merge(range(0,9), range('A', 'Z'), range('a', 'z')))), 0, $COOKIE_LENGTH);
}

function genCookie($sql, $user)
{
    global $COOKIE_PATH;
    $cookie = getRandomString();
    $sql->query("UPDATE `users` SET `lastsessionid`='" . $cookie . "' WHERE `id`='" . $user['id'] . "'");
    $file = fopen("${COOKIE_PATH}$cookie", "w");
    fwrite($file, $_SERVER['REMOTE_ADDR']);
    fclose($file);
    return $cookie;
}

function genId()
{
    return rand(1,100000000);
}

function req($arr, $a)
{
    global $ERR_INVALID_ARGUMENTS;
    foreach ($arr as $index => $value)
    {
        if (!isset($a[$value]))
            die_result(0, $ERR_INVALID_ARGUMENTS);
    }
}

?>
