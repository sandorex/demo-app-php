<?php
include './base.php';

if (isset($a['cookie']) && !isset($a['username']) && !isset($a['password']))
{
    $cookie = $a['cookie'];
    $cookie_file_path = "${COOKIE_PATH}$cookie";
    $cookie_exists = file_exists($cookie_file_path);

    if ($cookie_exists)
        $cookie_file_content = file_get_contents($cookie_file_path);
    else
        die_result(0, $ERR_INVALID_COOKIE);

    if ($cookie_exists && $cookie_file_content == $_SERVER['REMOTE_ADDR'])
    {
        if (time()-filemtime($cookie_file_path) > $COOKIE_TIMEOUT)
        {
            if (time()-filemtime($cookie_file_path) > $COOKIE_OVERWRITE_TIMEOUT)
            {
                unlink($cookie_file_path);
                die_result(0, $ERR_EXPIRED_COOKIE, array('changed' => TRUE));
            }
            die_result(0, $ERR_EXPIRED_COOKIE);
        }
        

        $file = fopen($cookie_file_path, "w");
        fwrite($file, $_SERVER['REMOTE_ADDR']);
        fclose($file);
        die_result(0, $SUCCESS, array('cookie' => $cookie));
    }
    else
    {
        if ($cookie_file_content != $_SERVER['REMOTE_ADDR'])
            die_result(0, $ERR_INVALID_COOKIE_OWNER);
        die_result(0, $ERR_INVALID_COOKIE);
    }
}
else
{
    req(array('username', 'password'), $a);
}

$username = $a['username'];
$password = $a['password'];

$sql = connectSQL();

$r = sqlsel($sql, "SELECT * FROM `users` WHERE `username`='$username'", "1.$ERR_INVALID_LOGIN");
if ($r[0]['password'] != $password)
    die_result(0, $ERR_INVALID_LOGIN);

$id = $r[0]['id'];
$cookie = $r[0]['lastsessionid'];
$cookie_file_path = "${COOKIE_PATH}$cookie";
$cookie_exists = file_exists($cookie_file_path);

if ($cookie_exists)
{
    $cookie_file_content = file_get_contents($cookie_file_path);
    if ($cookie_file_content != $_SERVER['REMOTE_ADDR'])
    {
        
        if (time()-filemtime($cookie_file_path) > $COOKIE_TIMEOUT)
        {
            if (time()-filemtime($cookie_file_path) > $COOKIE_OVERWRITE_TIMEOUT)
            {
                $cookie = genCookie($sql, $r[0]);
                die_result(0, $SUCCESS, array('cookie' => $cookie, 'changed' => TRUE));
            }
            die_result(0, $ERR_EXPIRED_COOKIE);
        }
        else
            die_result(0, $ERR_DUPLICATE_LOGIN);
    }
    elseif ($cookie_file_content == $_SERVER['REMOTE_ADDR'])
    {
        if (time()-filemtime($cookie_file_path) > $COOKIE_TIMEOUT)
        {
            $cookie = genCookie($sql, $r[0]);
            die_result(0, $SUCCESS, array('cookie' => $cookie, 'changed' => TRUE));
        }
        else
            die_result(0, $SUCCESS, array('cookie' => $cookie));
    }
    else
    {
        $cookie = genCookie($sql, $r[0]);
        die_result(0, $SUCCESS, array('cookie' => $cookie, 'changed' => TRUE));
    }
}
else
{
    $cookie = genCookie($sql, $r[0]);
    die_result(0, $SUCCESS, array('cookie' => $cookie, 'changed' => TRUE));
}

$sql->close();

?>
