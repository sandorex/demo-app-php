<?php
include 'base.php';

req(array('cookie', 'info'), $a);

$cookie = $a['cookie'];
$info = $a['info'];

$sql = connectSQL();
$sql_events = connectSQL('theapp_events');
$sql_friends = connectSQL('theapp_friends');

if ($info == $REQ_USER_INFO)
{
    $r = sqlsel($sql, "SELECT `id`, `username`, `lorem` FROM `users` WHERE `lastsessionid`='$cookie'")[0];    
    die_result(0, $SUCCESS, $r);
}
elseif ($info == $REQ_EVENTS)
{
    $r1 = sqlsel($sql, "SELECT `id` FROM `users` WHERE `lastsessionid`='$cookie'")[0];
    if ($r1 == NULL)
        die_result(0, $ERR_INVALID_COOKIE);
    $id = $r1['id'];
    $r2 = sqlsel($sql_events, "SELECT * FROM `$id`");
    if ($r2 == NULL)
        die_result(0, $SUCCESS, array());
    
    die_result(0, $SUCCESS, $r2);
}
elseif ($info == $REQ_FRIEND_LIST)
{
    $r1 = sqlsel($sql, "SELECT `id` FROM `users` WHERE `lastsessionid`='$cookie'")[0];
    if ($r1 == NULL)
        die_result(0, $ERR_INVALID_COOKIE);
    $id = $r1['id'];
    $r2 = sqlsel($sql_friends, "SELECT * FROM `$id`");
    if ($r2 == NULL)
        die_result(0, $SUCCESS, array());
    
    die_result(0, $SUCCESS, $r2);
}
else
    die_result(0, $ERR_INVALID_ARGUMENTS);

die_result(0, $ERR);

?>